package cwx.utils.ldaptodb.ldap;

import java.util.List;

public interface PersonDAO {
	public List<Person> getAllPersons();

	public List<Group> getAllGroups();
}
