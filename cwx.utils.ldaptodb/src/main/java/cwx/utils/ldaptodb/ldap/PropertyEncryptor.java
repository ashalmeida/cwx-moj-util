package cwx.utils.ldaptodb.ldap;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "singleton")
@Component
public class PropertyEncryptor {
	static public StringEncryptor stringEncryptor() {
		PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
		SimpleStringPBEConfig config = new SimpleStringPBEConfig();
		config.setPassword("x%2#YlyuMk4e");
		config.setAlgorithm("PBEWithMD5AndDES");
		config.setKeyObtentionIterations("1000");
		config.setPoolSize("1");
		config.setProviderName("SunJCE");
		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		config.setStringOutputType("base64");
		encryptor.setConfig(config);
		return encryptor;
	}
	//mojsitdb1 , 1521 , CWXDB , wcc_ocs M0js1t0426
	//mojprepdb1 , 1521 , CWXDB , wcc_ocs ,  p620Kbbmn1
	//wI8gVivWgX - ldap preprd wI8gVivWgX
	//p620Kbbmn1 - db pwd preprd /YfFsf5vIkZ
	
	public static void main(String[] args) {
		StringEncryptor encryptor = stringEncryptor();
		String message = "YfFsf5vIkZ";
		String encrypted = encryptor.encrypt(message);
		System.out.printf("Encrypted message %s\n", encrypted);
		String dencrypted = encryptor.decrypt(encrypted);
		System.out.printf("Encrypted message %s\n", dencrypted);
	}
}
