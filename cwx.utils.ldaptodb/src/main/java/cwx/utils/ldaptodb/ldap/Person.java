package cwx.utils.ldaptodb.ldap;

import java.util.Date;

public class Person {
	private String name;
	private String dn;
	private String displayName;
	private String lastName;
	private String firstName;
	private String mail;
	private String userID;
	private Date modifyTimestamp;
	private Date pwdChangedTime;
	private Date createTimestamp;

	public Date getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(Date modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}

	public Date getPwdChangedTime() {
		return pwdChangedTime;
	}

	public void setPwdChangedTime(Date pwdChangedTime) {
		this.pwdChangedTime = pwdChangedTime;
	}

	public Date getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(Date createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", dn=" + dn + ", displayName=" + displayName + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", mail=" + mail + ", userID=" + userID + ", modifyTimestamp="
				+ modifyTimestamp + ", pwdChangedTime=" + pwdChangedTime + ", createTimestamp=" + createTimestamp + "]";
	}

}
