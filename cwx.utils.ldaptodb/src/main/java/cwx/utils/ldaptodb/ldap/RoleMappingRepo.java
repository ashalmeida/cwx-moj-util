package cwx.utils.ldaptodb.ldap;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface RoleMappingRepo extends CrudRepository<RoleMapping, String> {
	List<RoleMapping> findAll();
}
