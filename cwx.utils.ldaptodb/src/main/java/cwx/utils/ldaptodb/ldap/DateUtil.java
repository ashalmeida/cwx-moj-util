package cwx.utils.ldaptodb.ldap;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	// sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

	public static Date getDate(String yyyyMMddHHmmss) {
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			return sdf.parse(yyyyMMddHHmmss);
		} catch (Exception e) {
		}
		return null;
	}
}
