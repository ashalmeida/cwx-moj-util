package cwx.utils.ldaptodb.ldap;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name = "CWX_USER_GROUP")
public class UserRoleDO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Version
	private Integer version;
	private String name;
	private String displayName;
	private String lastName;
	private String firstName;
	private String mail;
	private String userID;
	private String roleName;
	private Date createdOn;
	private Date updatedOn;
	private String dn;
	private String grpdn;
	private Date rmodifyTimestamp;
	private Date rcreateTimestamp;
	private Date umodifyTimestamp;
	private Date upwdChangedTime;
	private Date ucreateTimestamp;

	public Date getRmodifyTimestamp() {
		return rmodifyTimestamp;
	}

	public void setRmodifyTimestamp(Date rmodifyTimestamp) {
		this.rmodifyTimestamp = rmodifyTimestamp;
	}

	public Date getRcreateTimestamp() {
		return rcreateTimestamp;
	}

	public void setRcreateTimestamp(Date rcreateTimestamp) {
		this.rcreateTimestamp = rcreateTimestamp;
	}

	public Date getUmodifyTimestamp() {
		return umodifyTimestamp;
	}

	public void setUmodifyTimestamp(Date umodifyTimestamp) {
		this.umodifyTimestamp = umodifyTimestamp;
	}

	public Date getUpwdChangedTime() {
		return upwdChangedTime;
	}

	public void setUpwdChangedTime(Date upwdChangedTime) {
		this.upwdChangedTime = upwdChangedTime;
	}

	public Date getUcreateTimestamp() {
		return ucreateTimestamp;
	}

	public void setUcreateTimestamp(Date ucreateTimestamp) {
		this.ucreateTimestamp = ucreateTimestamp;
	}

	public String getGrpdn() {
		return grpdn;
	}

	public void setGrpdn(String grpdn) {
		this.grpdn = grpdn;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "UserRoleDO [id=" + id + ", version=" + version + ", name=" + name + ", displayName=" + displayName
				+ ", lastName=" + lastName + ", firstName=" + firstName + ", mail=" + mail + ", userID=" + userID
				+ ", roleName=" + roleName + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + ", dn=" + dn
				+ ", grpdn=" + grpdn + "]";
	}

}
