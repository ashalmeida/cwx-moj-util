package cwx.utils.ldaptodb.ldap;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.SearchControls;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Component;

@Component
public class PersonGroupDAOImpl implements PersonDAO {
	@Autowired
	private LdapTemplate ldapTemplate;
	@Value("${app.ldap.user.filter}")
	private String ldapUserFilter;
	@Value("${app.ldap.user.base}")
	private String ldapUserBase;
	@Value("${app.ldap.group.filter}")
	private String ldapGroupFilter;
	@Value("${app.ldap.group.base}")
	private String ldapGroupBase;
	private static final Logger logger = Logger.getLogger(PersonGroupDAOImpl.class);

	@Override
	public List<Person> getAllPersons() {
		logger.info("getAllPersons - Entering");
		logger.info("getAllPersons - USING ldapUserBase [" + ldapUserBase + " ] , ldapUserFilter [" + ldapUserFilter + "]");
		List<Person> persons = new ArrayList<Person>();
		try {

			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			controls.setReturningObjFlag(false);
			controls.setReturningAttributes(new String[] { "name", "displayname", "sn", "givenname", "mail", "uid", "pwdChangedTime", "modifyTimestamp", "createTimestamp" });
			List<Person> search = ldapTemplate.search(ldapUserBase, ldapUserFilter, controls, new PersonContextMapper());
			persons.addAll(search);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.info("getAllPersons - Exiting");
		return persons;
	}

	@Override
	public List<Group> getAllGroups() {
		logger.info("getAllGroups - Entering");
		logger.info("getAllPersons - USING ldapGroupBase [" + ldapGroupBase + " ] , ldapGroupFilter [" + ldapGroupFilter + "]");
		List<Group> groups = new ArrayList<Group>();
		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			controls.setReturningObjFlag(false);
			controls.setReturningAttributes(new String[] { "cn", "uniqueMember", "modifyTimestamp", "createTimestamp" });

			List<Group> search = ldapTemplate.search(ldapGroupBase, ldapGroupFilter, controls, new GroupAttributesMapper());
			groups.addAll(search);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.info("getAllGroups - Exiting");
		return groups;
	}

}
