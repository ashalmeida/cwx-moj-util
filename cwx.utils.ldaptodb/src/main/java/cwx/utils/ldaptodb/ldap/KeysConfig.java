package cwx.utils.ldaptodb.ldap;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KeysConfig {

	private static final Logger logger = Logger.getLogger(KeysConfig.class);
	@Autowired
	@Qualifier("jasyptStringEncryptor")
	private StringEncryptor stringEncryptor;
	@Value("${app.uecredential}")
	private String uecredential;

	@PostConstruct
	public void onStartUp() {
		logger.info("onStartUp - uecredential [" + uecredential + " ]  ");
		if (uecredential != null) {
			String[] uecredentialarray = uecredential.split(",");
			for (String uecredentialvar : uecredentialarray) {
				logger.info("onStartUp - " + uecredentialvar + " = " + stringEncryptor.encrypt(uecredentialvar));
			}
		}

	}

}
