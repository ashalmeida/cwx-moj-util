package cwx.utils.ldaptodb.ldap;

import org.springframework.data.repository.CrudRepository;

public interface UserPGPMapRepo extends CrudRepository<UserPGPMap, String> {

}
