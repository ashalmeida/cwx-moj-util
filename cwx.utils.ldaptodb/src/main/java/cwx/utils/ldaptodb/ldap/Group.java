package cwx.utils.ldaptodb.ldap;

import java.util.Arrays;
import java.util.Date;

public class Group {
	private String description;
	private String dn;
	private String[] uniqueMembers;
	private Date modifyTimestamp;
	private Date createTimestamp;

	public Date getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(Date modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}

	public Date getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(Date createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getUniqueMembers() {
		return uniqueMembers;
	}

	public void setUniqueMembers(String[] uniqueMembers) {
		this.uniqueMembers = uniqueMembers;
	}

	@Override
	public String toString() {
		return "Group [description=" + description + ", dn=" + dn + ", uniqueMembers=" + Arrays.toString(uniqueMembers)
				+ ", modifyTimestamp=" + modifyTimestamp + ", createTimestamp=" + createTimestamp + "]";
	}

}
