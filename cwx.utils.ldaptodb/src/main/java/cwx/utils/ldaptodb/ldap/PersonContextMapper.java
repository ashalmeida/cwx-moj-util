package cwx.utils.ldaptodb.ldap;

import org.apache.log4j.Logger;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;

public class PersonContextMapper implements ContextMapper<Person> {
	private static final Logger logger = Logger.getLogger(PersonContextMapper.class);

	public Person mapFromContext(Object ctx) {
		DirContextAdapter context = (DirContextAdapter) ctx;
		if (logger.isDebugEnabled()) {
			logger.debug("ADDING ::" + context.getDn().toString().toLowerCase());
		}
		Person person = new Person();
		try {
			person.setDn(context.getDn().toString().toLowerCase());
			person.setName(context.getStringAttribute("name"));
			person.setDisplayName(context.getStringAttribute("displayname"));
			person.setLastName(context.getStringAttribute("sn"));
			person.setFirstName(context.getStringAttribute("givenname"));
			person.setMail(context.getStringAttribute("mail"));
			person.setUserID(context.getStringAttribute("uid"));
			person.setModifyTimestamp(DateUtil.getDate(context.getStringAttribute("modifyTimestamp")));
			person.setPwdChangedTime(DateUtil.getDate(context.getStringAttribute("pwdChangedTime")));
			person.setCreateTimestamp(DateUtil.getDate(context.getStringAttribute("createTimestamp")));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.info("ADDING ::" + person);
		return person;
	}
}
