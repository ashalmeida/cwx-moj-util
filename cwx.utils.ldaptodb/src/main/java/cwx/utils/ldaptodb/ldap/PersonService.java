package cwx.utils.ldaptodb.ldap;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonService {
	private static final Logger logger = Logger.getLogger(PersonService.class);
	@Autowired
	private PersonDAO personDAO;
	@Autowired
	private UserRoleDORepo userRoleDORepo;
	@Autowired
	private RoleMappingRepo roleMappingRepo;
	@Autowired
	private UserPGPMapRepo userPGPMapRepo;

	public void onStartUpTemp() {
		logger.info("onStartUp - entering");
		try {
			logger.info("getAllPersons - starting");
			List<Person> persons = personDAO.getAllPersons();
			// Date theDate = sdf.parse(persons.get(0).getCreateTimestamp());
			// logger.info("theDate - " + theDate);
			logger.info("getAllPersons - completed");
			logger.info("getAllGroups - starting");
			List<Group> groups = personDAO.getAllGroups();
			// theDate = sdf.parse(groups.get(0).getCreateTimestamp());
			// logger.info("theDate - " + theDate);
			logger.info("getAllGroups - completed ");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@PostConstruct
	public void onStartUp() {
		logger.info("onStartUp - entering");
		try {
			logger.info("archive_user_role_mapping - started");
			userRoleDORepo.archive_user_role_mapping();
			logger.info("archive_user_role_mapping - completed");
			logger.info("getAllPersons - starting");
			List<Person> persons = personDAO.getAllPersons();
			logger.info("getAllPersons - completed");
			Map<String, Person> personMap = new HashMap<String, Person>();
			for (Person person : persons) {
				if (logger.isDebugEnabled()) {
					logger.debug(person);
				}
				personMap.put(person.getDn().toLowerCase(), person);
			}
			int noOfRecords = 0;
			logger.info("getAllGroups - starting");
			List<Group> groups = personDAO.getAllGroups();
			logger.info("getAllGroups - completed ");
			Map<String, List<String>> personRoleListMap = new HashMap<String, List<String>>();
			for (Group group : groups) {
				logger.info(group.getDn() + " , memberCount =  " + (group.getUniqueMembers() == null ? "0" : group.getUniqueMembers().length));
				if (group.getUniqueMembers() != null) {
					for (String uniqueMember : group.getUniqueMembers()) {
						Person person = personMap.get(uniqueMember.toLowerCase());
						if (person != null) {
							if (logger.isDebugEnabled()) {
								logger.debug("ADDING :: " + person);
							}
							List<String> roles = personRoleListMap.get(person.getUserID());
							if (roles == null) {
								roles = new ArrayList<>();
								roles.add(group.getDescription());
								personRoleListMap.put(person.getUserID(), roles);
							} else {
								roles.add(group.getDescription());
							}
							UserRoleDO userRoleDO = new UserRoleDO();
							userRoleDO.setRoleName(group.getDescription());
							userRoleDO.setDisplayName(person.getDisplayName());
							userRoleDO.setDn(person.getDn());
							userRoleDO.setFirstName(person.getFirstName());
							userRoleDO.setLastName(person.getLastName());
							userRoleDO.setMail(person.getMail());
							userRoleDO.setName(person.getName());
							userRoleDO.setUserID(person.getUserID());
							userRoleDO.setGrpdn(group.getDn());
							userRoleDO.setUcreateTimestamp(person.getCreateTimestamp());
							userRoleDO.setUmodifyTimestamp(person.getModifyTimestamp());
							userRoleDO.setUpwdChangedTime(person.getPwdChangedTime());
							userRoleDO.setRcreateTimestamp(group.getCreateTimestamp());
							userRoleDO.setRmodifyTimestamp(group.getModifyTimestamp());
							userRoleDORepo.save(userRoleDO);
							if (logger.isDebugEnabled()) {
								logger.debug("ADDED :: " + userRoleDO);
							}
							noOfRecords++;
						} else {
							logger.debug("NOT FOUND :: " + group.getDn() + " >> " + uniqueMember);
						}

					}
				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("ADDING EMPTY ROLE :: " + group);
					}
					UserRoleDO userRoleDO = new UserRoleDO();
					userRoleDO.setRoleName(group.getDescription());
					userRoleDORepo.save(userRoleDO);
					noOfRecords++;
					if (logger.isDebugEnabled()) {
						logger.debug("ADDED :: " + userRoleDO);
					}
				}
			}
			List<RoleMapping> roleMappings = roleMappingRepo.findAll();
			Map<String, RoleMapping> roleMappingMap = new HashMap<String, RoleMapping>();
			for (RoleMapping roleMapping : roleMappings) {
				roleMappingMap.put(roleMapping.getRoleName(), roleMapping);
			}
			int userCount = 0;
			for (Map.Entry<String, List<String>> entry : personRoleListMap.entrySet()) {
				try {
					if (entry.getKey() != null) {
						UserPGPMap userPGPMap = new UserPGPMap();
						userPGPMap.setUserID(entry.getKey());
						userPGPMap.setRank(-1);
						userPGPMap.setRoleName("UNDEFINED");
						List<String> roles = entry.getValue();
						for (String role : roles) {
							RoleMapping roleMapping = roleMappingMap.get(role);
							if (roleMapping != null) {
								if (userPGPMap.getRank() < roleMapping.getRank()) {
									userPGPMap.setRoleName(roleMapping.getRoleName());
									userPGPMap.setRank(roleMapping.getRank());
								}
							}
						}
						userPGPMapRepo.save(userPGPMap);
						userCount++;
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
			logger.info("ADDED :: " + noOfRecords + " , userCount = " + userCount);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
