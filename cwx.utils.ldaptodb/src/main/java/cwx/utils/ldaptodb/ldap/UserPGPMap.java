package cwx.utils.ldaptodb.ldap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "CWX_USER_PGP_MAP")
public class UserPGPMap {
	@Id
	@Column(name = "USERID")
	private String userID;
	@Column(name = "ROLE_NAME")
	private String roleName;
	@Transient
	private Integer rank;

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
