package cwx.utils.ldaptodb.ldap;

import org.apache.log4j.Logger;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;

public class GroupAttributesMapper implements ContextMapper<Group> {
	private static final Logger logger = Logger.getLogger(GroupAttributesMapper.class);

	public Group mapFromContext(Object ctx) {

		Group group = new Group();
		try {
			DirContextAdapter context = (DirContextAdapter) ctx;
			group.setDn(context.getDn().toString().toLowerCase());
			group.setDescription(context.getStringAttribute("cn"));
			group.setUniqueMembers(context.getStringAttributes("uniqueMember"));
			group.setModifyTimestamp(DateUtil.getDate(context.getStringAttribute("modifyTimestamp")));
			group.setCreateTimestamp(DateUtil.getDate(context.getStringAttribute("createTimestamp")));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.info("ADDING ::" + group);
		return group;
	}
}
