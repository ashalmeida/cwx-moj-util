  package cwx.utils.ldaptodb.ldap;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleDORepo extends CrudRepository<UserRoleDO, Long> {
	@Query(value = "SELECT archive_user_role_mapping() from dual", nativeQuery = true)
	String archive_user_role_mapping();
}
