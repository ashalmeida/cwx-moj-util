package cwx.utils.ldaptodb.ldap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "CWX_ROLE_GROUP_PARTITION")
public class RoleMapping {
	@Id
	@Column(name = "ROLE_NAME")
	private String roleName;
	@Column(name = "PARTITION")
	private String partition;
	@Column(name = "GROUP_NAME")
	private String groupName;
	@Column(name = "PROFILE")
	private String profile;
	@Column(name = "RANK")
	private Integer rank;

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getPartition() {
		return partition;
	}

	public void setPartition(String partition) {
		this.partition = partition;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
}
