package cwx.utils.ldaptodb.ldap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class LDAPConfig {
	@Value("${app.ldap.url}")
	private String ldapUrl;
	@Value("${app.ldap.base}")
	private String ldapbase;
	@Value("${app.ldap.userdn}")
	private String ldapuserdn;
	@Value("${app.ldap.credential}")
	private String ldapcredential;
	private static final Logger logger = Logger.getLogger(LDAPConfig.class);

	@Bean
	public LdapContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(ldapUrl);
		contextSource.setBase(ldapbase);
		contextSource.setUserDn(ldapuserdn);
		contextSource.setPassword(ldapcredential);
		contextSource.afterPropertiesSet();
		logger.info("ldapUrl = " + ldapUrl + " , ldapuserdn = " + ldapuserdn + " , ldapcredential = "
				+ ldapcredential.length());
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		logger.info("ldapUrl = " + ldapUrl + " , ldapuserdn = " + ldapuserdn + " , ldapcredential = "
				+ ldapcredential.length());
		return new LdapTemplate(contextSource());
	}
}
