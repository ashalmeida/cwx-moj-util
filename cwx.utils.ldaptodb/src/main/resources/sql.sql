-- SELECT archive_user_role_mapping() from dual;

DROP SEQUENCE cwx_user_group_sequence;
DROP table CWX_USER_GROUP;
DROP table CWX_USER_GROUP_HISTORY;

CREATE SEQUENCE cwx_user_group_sequence INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 CACHE 20;

CREATE TABLE CWX_USER_GROUP 
(
  ID NUMBER(19, 0) DEFAULT cwx_user_group_sequence.NEXTVAL
, CREATED_ON TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP 
, DESCRIPTION VARCHAR2(255 CHAR) 
, DISPLAY_NAME VARCHAR2(255 CHAR) 
, FIRST_NAME VARCHAR2(255 CHAR) 
, LAST_NAME VARCHAR2(255 CHAR) 
, MAIL VARCHAR2(255 CHAR) 
, NAME VARCHAR2(255 CHAR) 
, UPDATED_ON TIMESTAMP(6) 
, USERID VARCHAR2(255 CHAR) 
, VERSION NUMBER(10, 0) 
, DN CLOB 
, GRPDN CLOB 
, CONSTRAINT CWX_USER_GROUP_PK PRIMARY KEY 
  (
    ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX CWX_USER_GROUP_PK ON CWX_USER_GROUP (ID ASC) 
      LOGGING 
      TABLESPACE CWX 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        NEXT 1048576 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
);

CREATE TABLE CWX_USER_GROUP_HISTORY 
(
  ID NUMBER(19, 0) NOT NULL 
, CREATED_ON TIMESTAMP(6) 
, DESCRIPTION VARCHAR2(255 CHAR) 
, DISPLAY_NAME VARCHAR2(255 CHAR) 
, FIRST_NAME VARCHAR2(255 CHAR) 
, LAST_NAME VARCHAR2(255 CHAR) 
, MAIL VARCHAR2(255 CHAR) 
, NAME VARCHAR2(255 CHAR) 
, UPDATED_ON TIMESTAMP(6) 
, USERID VARCHAR2(255 CHAR) 
, VERSION NUMBER(10, 0) 
, PARENTID NUMBER 
, SNAPSHOTDATE TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP 
, DN CLOB 
, GRPDN CLOB 
, CONSTRAINT CWX_USER_GROUP_HISTORY_PK PRIMARY KEY 
  (
    ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX CWX_USER_GROUP_HISTORY_PK ON CWX_USER_GROUP_HISTORY (ID ASC) 
      LOGGING 
      TABLESPACE CWX 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        NEXT 1048576 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
); 


CREATE OR REPLACE FUNCTION archive_user_role_mapping RETURN VARCHAR2 AS

    CURSOR provider_plan_details IS
    SELECT
        created_on,
        description,
        display_name,
        first_name,
        last_name,
        mail,
        name,
        updated_on,
        userid,
        dn,
        grpdn,
        version,
        id
    FROM
        cwx_user_group;

    TYPE provider_plan_details_type IS
        TABLE OF provider_plan_details%rowtype;
    plan_provider_dtls_cur   provider_plan_details_type;
    PRAGMA autonomous_transaction;
BEGIN
    OPEN provider_plan_details;
    FETCH provider_plan_details BULK COLLECT INTO plan_provider_dtls_cur;
    CLOSE provider_plan_details;
    IF plan_provider_dtls_cur.count > 0 THEN
        FOR item IN plan_provider_dtls_cur.first..plan_provider_dtls_cur.last LOOP
            INSERT INTO cwx_user_group_history VALUES (
                plan_provider_dtls_cur(item).id,
                plan_provider_dtls_cur(item).created_on,
                plan_provider_dtls_cur(item).description,
                plan_provider_dtls_cur(item).display_name,
                plan_provider_dtls_cur(item).first_name,
                plan_provider_dtls_cur(item).last_name,
                plan_provider_dtls_cur(item).mail,
                plan_provider_dtls_cur(item).name,
                plan_provider_dtls_cur(item).updated_on,
                plan_provider_dtls_cur(item).userid,
                plan_provider_dtls_cur(item).version,
                plan_provider_dtls_cur(item).id,
                systimestamp,
                plan_provider_dtls_cur(item).dn,
                plan_provider_dtls_cur(item).grpdn
            );
          
            DELETE FROM cwx_user_group
            WHERE
                id = plan_provider_dtls_cur(item).id;

            COMMIT;
        END LOOP;
    END IF;

    COMMIT;
    RETURN 'SUCCESS';
END archive_user_role_mapping;

