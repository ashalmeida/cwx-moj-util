SELECT
    activeusers.id,
    activeusers.created_on,
    activeusers.role_name,
    activeusers.display_name,
    activeusers.first_name,
    activeusers.last_name,
    activeusers.mail,
    activeusers.name,
    activeusers.updated_on,
    lower(activeusers.userid) userid,
    lower(activeusers.userid) || '_' || lower(activeusers.role_name) userroleid,
    lower(activeusers.userid) || '_' || lower(activeusers.role_name) record_id,
    activeusers.version,
    lower(activeusers.dn) userdn,
    lower(activeusers.grpdn) groupdn,
    activeusers.rcreate_timestamp,
    activeusers.rmodify_timestamp,
    activeusers.upwd_changed_time,
    activeusers.ucreate_timestamp,
    activeusers.umodify_timestamp,
    extract(day  FROM SYSTIMESTAMP - ucreate_timestamp )  AGE,
    extract(day  FROM SYSTIMESTAMP - rcreate_timestamp )  RAGE,
    (select eventdate from sctaccesslog where sc_scs_idcservice = 'GET_SEARCH_RESULTS' and extfield_5 = 'score' AND comp_username = activeusers.userid  and extfield_3 is null and extfield_4 is null   order by eventdate desc FETCH NEXT 1 ROWS ONLY ) last_login_time, 
    role_mappings.group_name group_name ,
    --role_mappings.partition partition_name,
    profile_partition.partition partition_name,
    -- role_mappings.profile profile ,
    profile_partition.profile profile
   
FROM
    cwx_user_group activeusers 
    LEFT OUTER JOIN CWX_USER_PGP_MAP pgp_map ON lower(activeusers.userid) = lower(pgp_map.userid)
    LEFT OUTER JOIN  CWX_ROLE_GROUP_PARTITION role_mappings ON lower(pgp_map.role_name) = lower(role_mappings.role_name)
    LEFT OUTER JOIN  CWX_PROFILE_PARTITION profile_partition ON lower(profile_partition.profile) = lower(role_mappings.profile)
 
 -- WHERE role_mappings.profile = 'Judicial Support Officers' 
 
 ORDER BY activeusers.display_name ASC