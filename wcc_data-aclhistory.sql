SELECT history.id,
    history.created_on,
    history.role_name,
    history.display_name,
    history.first_name,
    history.last_name,
    history.mail,
    history.name,
    history.updated_on,
    lower(history.userid) userid,
    lower(history.userid) || '_' || lower(history.role_name) userroleid,
    lower(history.userid) || '_' || lower(history.role_name) || '_' || TO_CHAR(history.snapshotdate , 'DD_MM_YYYY') record_id,
    history.version,
    lower(history.dn) userdn,
    lower(history.grpdn) groupdn , 
    TO_DATE(TO_CHAR(history.snapshotdate , 'DD-MM-YYYY'), 'DD-MM-YYYY') AS ASOFDATE    
FROM  
    cwx_user_group_history history  
ORDER BY 
  ASOFDATE , userroleid ASC